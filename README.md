<!--
SPDX-FileCopyrightText: 2020 Adriel Dumas- -Jondeau <adrieldj@orange.fr>
SPDX-License-Identifier: GFDL-1.3-or-later
-->

# XMPP : Fiche pratique

Pour voir la version générée, voir le [wiki](https://codeberg.org/leirda/presxmpp/wiki).
